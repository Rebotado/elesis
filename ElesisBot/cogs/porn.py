from discord.ext import commands
from extensions import pornhub
import json
import math
import re
import discord
from random import randint


class Porn(commands.Cog):

    def __init__(self, bot):
        self.bot = bot


    @commands.group()
    async def porn(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send('Elije una funcion: video, star, image. Ej. %porn video mia khalifa')

    @porn.command()
    async def video(self, ctx, *, args: str):

        if ',' in args:
            keywords , filters = args.split(',')
            keywords = keywords.split(' ') 
            filters = await self._ConvertToDict(filters)
        else:
            keywords , filters = args.split(' ') , None

        videos = await self._GetVideos(keywords, filters)
        print(videos)

        print(f'filters: {filters}')
        print(f'keywords: {keywords}')

        for video in videos:
            await ctx.send(video['url'])

    @porn.command()
    async def photo(self, ctx, *, args:str):
        embed = discord.Embed()
        number = 1
        if ',' in args:
            keywords , number = args.split(',')
            keywords = keywords.split(' ')
            if 'number=' in number:
                number = int(number.replace('number=' , ''))
            else:
                number = int(number)
        else:
            keywords = args.split(' ')
        client = pornhub.PornHub(keywords=keywords)
        for photo in client.getPhotos(number):
            embed.set_image(url=photo)
            await ctx.send(embed=embed)

    @porn.group()
    async def star(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send('Not implemented')



    @star.command()
    async def random(self, ctx, number:int = 1):
        client = pornhub.PornHub()
        number = number if number <= 10 else 10
        for x in range(number):
            page = randint(1, 50)
            position = randint(1, 54)
            star = list(client.getStars(position, page=page))[-1]
            embed = discord.Embed()
            embed.set_image(url=star['photo'])
            embed.set_author(name=star['name'], url=star['url'])
            embed.add_field(name='videos' , value=star['videos'])
            await ctx.send(embed=embed)

    @star.command()
    async def top(self, ctx, number:int = 1):
        number = number if number < 100 else 100

        client = pornhub.PornHub()

        star = list(client.getStars(number))[number - 1]
        embed = discord.Embed()
        embed.set_image(url=star['photo'])
        embed.set_author(name=star['name'], url=star['url'])
        embed.add_field(name='videos' , value=star['videos'])
        await ctx.send(embed=embed)



    #Self commands
    async def _ConvertToDict(self, input):
        filter_dict = {}
        filter_input = re.split(';|=',input.replace(' ' , ''))
        print(filter_input)
        for x in range(1, len(filter_input), 2):
            filter_dict[filter_input[x-1]] = filter_input[x]
        return filter_dict

    async def _GetVideos(self, keywords, filters=None):
        client = pornhub.PornHub(keywords=keywords)
        number = 1
        rating = 0
        duration = 0
        videos = []
        if filters is not None:
            number = int(filters['number']) if 'number' in filters else 1
            rating = int(filters['rating']) if 'rating' in filters else 0
            duration = int(filters['duration'].replace(':', '')) if 'duration' in filters else 0

        video_count = 0
        page_count = 1
        while video_count < number:
            page_videos = list(client.getVideos(25, page=page_count))[4:]
            page_videos = list(filter(lambda x: x['rating'] > rating and int(x['duration'].replace(':','')) > duration, page_videos))
            videos.extend(page_videos)
            video_count += len(page_videos)
            page_count += 1

        return videos[:number]

def setup(bot):
    bot.add_cog(Porn(bot))