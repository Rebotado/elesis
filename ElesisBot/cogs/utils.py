from discord.ext import commands



class Utils(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    

    @commands.command(name='clear')
    async def clear(self, ctx, number:int):
        number = number if number < 100 else 100
        deleted = await ctx.channel.purge(limit=number+1)
        await ctx.send(f'Se borraron {len(deleted)-1} mensajes')



def setup(bot):
    bot.add_cog(Utils(bot))