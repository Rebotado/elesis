import discord
from discord.ext import commands
import config
import sys, traceback

def get_prefix(bot, message):
    prefixes = ['?' , '!' , '%']

    if not message.guild:
        return '?'
    
    return commands.when_mentioned_or(*prefixes)(bot, message)

initial_extensions = [
    'cogs.admin' , 'cogs.porn' , 'cogs.utils' , 'cogs.hentai' , 'cogs.musicplayer' , 'cogs.trash'
]



bot = commands.Bot(command_prefix=get_prefix, description='Elesis bot')

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print(f'Failed to load extension {extension}.' , file=sys.stderr)
            traceback.print_exc()


@bot.event
async def on_ready():

    print(f'\n\nLogged in as: {bot.user.name} - {bot.user.id}\nVersion: {discord.__version__}\n')
    for extension in bot.extensions:
        print(f'Loaded extension {extension}')

    await bot.change_presence(activity=discord.Activity(name='Elesis bot', type=discord.ActivityType.playing))
    print(f'Succesfully logged in and booted...!')



bot.run(config.TOKEN, bot=True, reconnect=True)