import string
import random

HOST_URL = 'https://hanime.tv/'

VIDEO_URL = 'https://hanime.tv/videos/hentai/'

SEARCH_URL = 'https://search.hanime.tv/'

BROWSE_TRENDING_URL = 'https://hanime.tv/api/v3/browse/trending'

BROWSE_IMAGE_URL = 'https://hanime.tv/api/v3/community_uploads?'




IMAGE_FILTERS = {
    'media': 'media' ,
    'nsfw' : 'nsfw_general',
    'furry' : 'furry',
    'yaoi' : 'yaoi',
    'yuri' : 'yuri',
    '3d' : 'irl-3d',
    'futa': 'futa'
}



def seed_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


class NoPagesFound(ValueError):
    pass