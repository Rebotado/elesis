import requests
import sys
from .core import *
import re
try:
    import ujson as json
except:
    import json

import random
from math import ceil

'''
Author : Moe Poi <moepoi@protonmail.com>
License MIT
'''

class HanimeTV:

    def search_videos(self, query, number=1):

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
            'Accept': 'application/json, text/plain, */*',
            'Accept-Language': 'en-US,en;q=0.5',
            'content-type': 'application/json',
            'Origin': HOST_URL,
            'Connection': 'keep-alive',
            'TE': 'Trailers'
            }
        data = {
            "search_text": str(query),
            "tags": [],
            "tags_mode": "OR",
            "brands": [],
            "blacklist": [],
            "order_by": "created_at_unix",
            "ordering": "desc",
            "page": 0
        }
        req = requests.post(SEARCH_URL, headers=headers, json=data)
        pages = int(json.loads(req.text)['nbPages'])
        if pages == 0 or int(json.loads(req.text)['nbHits']) == 0:
            raise NoPagesFound
        
        videos_found = 0
        for page in range(pages):
            data['page'] = page
            req = requests.post(SEARCH_URL, headers=headers, json=data)
            response = json.loads(json.loads(req.text)['hits'])
            for data in response:
                if number > videos_found:
                    yield self._fixHentaiData(data)
                    videos_found += 1
                else:
                    raise StopIteration
            
    def search_images(self, number=1, filters=['nsfw']):
        url = self._generateImageBaseUrl(filters)
        offset = 0
        order = 'created_at,DESC'

        #Get total images found with filters and images per page.
        #Then get total pages
        request_url = self._generateImageUrl(url, offset, order)
        meta_data = self._getImageUrlData(request_url)['meta']
        total_images = int(meta_data['total'])
        images_per_page = int(meta_data['count'])
        total_pages = ceil(total_images / images_per_page)

        images_found = 0

        for page in range(total_pages):
            request_url = self._generateImageUrl(url, offset, order)
            response = self._getImageUrlData(request_url)['data']

            for data in response:
                if number > images_found:
                    yield data
                    images_found += 1
                else:
                    raise StopIteration
            
            offset += images_per_page



    def random_images(self, number=1, filters=['nsfw']):
        url = self._generateImageBaseUrl(filters)
        offset = 0
        order = 'created_at,DESC'

        #Get total images found with filters and images per page.
        #Then get total pages
        request_url = self._generateImageUrl(url, offset, order)
        meta_data = self._getImageUrlData(request_url)['meta']
        total_images = int(meta_data['total'])
        images_per_page = int(meta_data['count'])
        total_pages = ceil(total_images / images_per_page)

        images_found = 0

        for page in range(total_pages):
            offset = random.randint(0, total_pages) * 24
            request_url = self._generateImageUrl(url, offset, order)
            response = self._getImageUrlData(request_url)['data']
            reponse = random.choices(response)
            for data in response:
                if number > images_found:
                    yield data
                    images_found += 1
                else:
                    raise StopIteration
            
            offset += images_per_page



    def random_videos(self, number=1):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Language': 'en-US,en;q=0.5',
            'Connection': 'keep-alive',
            'TE': 'Trailers',
            'path': '/api/v3/browse/trending/',
            'x-directive': 'api'
        }
        data = {
            'time': 'month',
            'page': '1'
        }

        req = requests.get(BROWSE_TRENDING_URL, headers=headers, data=data)
        response = json.loads(req.text)
        pages = int(response['number_of_pages'])
        videos_per_page = int(response['page_size'])
        videos_found = 0

        while number > videos_found:
            page = random.randint(0, pages)
            video_position = random.randint(0, videos_per_page - 1)
            url = self._craftVideoUrl(page, 'month')
            req = requests.get(url, headers=headers, data=data)
            response = json.loads(req.text)['hentai_videos'][video_position]
            yield self._fixHentaiData(response)
            videos_found += 1

    def _fixHentaiData(self, data):
        data['url'] = VIDEO_URL + data['slug']
        if 'description' in data:
            description = data['description'] 
            data['description'] = re.sub(r'<p>|</p>|\n','', description)
        return data


    def _generateImageUrl(self, url, offset, order):
        return url + f'__offset={offset}&__order={order}'


    def _generateImageBaseUrl(self, filters):
        if filters is None:
            filters = ['nsfw']

        #Generate base url with filters
        url = BROWSE_IMAGE_URL
        for filter in filters:
            if filter in IMAGE_FILTERS:
                url += 'channel_name__in[]=' + IMAGE_FILTERS[filter] + '&'
            else:
                raise Exception('Filtro no valido')

        return url

    def _getImageUrlData(self, request_url):
        headers = {
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'accept-Language': 'en-US,en;q=0.5',
            'content-type': 'application/json',
            'path': '/api/v3/community_uploads',
            'x-directive': 'api',
            'x-time': '0'
            }

        req = requests.get(request_url, headers=headers)
        return json.loads(req.text)


    def _craftVideoUrl(self, page, time):
        url = BROWSE_TRENDING_URL + f'?time={time}&page={page}'
        return url
