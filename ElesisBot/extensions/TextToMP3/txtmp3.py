import requests
from .core import *
import json
import os

class TextToMp3:


    def __init__(self, text):
        self.text = text

    def request_mp3(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0',
            'Origin' : ORIGIN_URL,
            'Connection': 'keep-alive',
            'Host': 'ttsmp3.com',
            'Referer': ORIGIN_URL,
        }


        data = {
            'lang' : 'Mia',
            'source' : 'ttsmp3',
            'msg' : self.text
        }


        req = requests.post(REQUEST_URL, headers=headers, data=data)
        url = json.loads(req.text)['URL']

        return url