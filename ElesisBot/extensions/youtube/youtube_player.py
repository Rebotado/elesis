from pytube import YouTube , Playlist

class YouTubePlayer:


    playlist = []



    async def addSong(self, url):
        self.playlist.append(url)

    async def addPlaylist(self, url):
        playlist =  Playlist(url)
        playlist.populate_video_urls()
        self.playlist.extend(playlist.video_urls)

    async def play(self):
        url = self.playlist.pop(0)
        try:
            url = YouTube(url).streams.filter(only_audio=True).first().url
        except:
            raise Exception
        return url






